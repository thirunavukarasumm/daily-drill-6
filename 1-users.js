/*

Q1 Find all users who are interested in playing video games.
Q2 Find all users staying in Germany.
Q3 Sort users based on their seniority level 
   for Designation - Senior Developer > Developer > Intern
   for Age - 20 > 10
Q4 Find all users with masters Degree.
Q5 Group users based on their Programming language mentioned in their designation.


*/ 

let users = require("./dataset")

function usersInterestedInVideoGames(users){
    return Object.entries(users).reduce((accu,user)=>{
        let checkVideoGames;
        if (user[1].interests !== undefined){
            checkVideoGames = user[1].interests.toString().includes("video games");
        }else{
            checkVideoGames = user[1].interest.toString().includes("video games");
        }
        if (checkVideoGames) {
            accu[user[0]] = user[1];
        }
        return accu
        
    },{})
}

// console.log(usersInterestedInVideoGames(users));


function findUserNationality(users, country) {
    return Object.entries(users).reduce((accu, user) => {
        if (user[1].nationality === country) {
            accu[user[0]] = user[1];
        }
        return accu
    }, {})
}

// console.log(findUserNationality(users,"Germany"));

function findMastersDegree(users) {
    return Object.entries(users).filter((user) => {
        return user[1].qualification.includes("Masters")
    })
}

// console.log(findMastersDegree(users));

function sortUser(users, designation) {
    if (designation) {
        return Object.entries(users).map((user) => {
            user[1]["name"] = user[0];
            return user[1];
        }).sort((person1, person2) => {
            return person2.desgination.localeCompare(person1.desgination)
        })
    } else {
        return Object.entries(users).map((user) => {
            user[1]["name"] = user[0];
            return user[1];
        }).sort((person1, person2) => {
            return person2.age - person1.age;
        })
    }

}
console.log(sortUser(users, true));